<?php

class Client
{
    private $firstName;
    private $lastName;
    private $email;
    private $phoneNumber1;
    private $phoneNumber2;
    private $comment;
    
    public function __construct(
            string $firstName, 
            string $lastName, 
            string $email, 
            string $phoneNumber1, 
            string $phoneNumber2, 
            string $comment
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->phoneNumber1 = $phoneNumber1;
        $this->phoneNumber2 = $phoneNumber2;
        $this->comment = $comment;
    }
        
    public function errorsExist()
    {
        $errors = [];
        
        if (empty($this->firstName)) {
            $errors[] = "\nFirst name is not valid";
        }
        if(strlen($this->firstName)>20){
            $errors[] = "\nFirst name is too long";
        }
            
        if (empty($this->lastName)) {
            $errors[] = "\nLast name is not valid";
        }
         if(strlen($this->lastName)>20){
            $errors[] = "\nLast name is too long";
        }
           
        if (empty($this->email)) {
            $errors[] = "\nEmail is not valid";
        }
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = "\nEmail format not valid";
        }
        
        if (empty($this->phoneNumber1)) {
            $errors[] = "\nPhone number 1 is not valid";
        }
        if (!is_numeric($this->phoneNumber1)) {
            $errors[] = "\nPhone number 1 is not valid";  
        }
        if(strlen((string)$this->phoneNumber1) != 9) {
            $errors[] = "\nPhone number 1 wrong length";
        }

        if (empty($this->phoneNumber2)) {
            $errors[] = "\nPhone number 2 is not valid";
        }
        if (!is_numeric($this->phoneNumber2)) {
            $errors[] = "\nPhone number 2 is not valid";  
        }
        if(strlen((string)$this->phoneNumber2) != 9) {
            $errors[] = "\nPhone number 2 wrong length";
        }
         if (empty($this->comment)) {
            $errors[] = "\nComment is not valid";
        }
        if(strlen($this->comment)>100){
            $errors[] = "\nComment is too long";
        }
     
        if (!empty($errors)) {
            return $errors;
        }
        
        return false;
    }
    
    
    
    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPhoneNumber1() {
        return $this->phoneNumber1;
    }

    public function getPhoneNumber2() {
        return $this->phoneNumber2;
    }

    public function getComment() {
        return $this->comment;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPhoneNumber1($phoneNumber1) {
        $this->phoneNumber1 = $phoneNumber1;
    }

    public function setPhoneNumber2($phoneNumber2) {
        $this->phoneNumber2 = $phoneNumber2;
    }

    public function setComment($comment) {
        $this->comment = $comment;
    }
}
