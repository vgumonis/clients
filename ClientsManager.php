<?php
require 'Client.php';

class ClientsManager
{
    private $clients = [];
    
    public function start()
    {
        while (true) {
            $option = $this->handleOptions();
            switch ($option){
            case 1: 
                $this->printAllClients();
                continue;
            case 2:
                $this->createNewClientFromTerminal();
                continue;
            case 3: 
                $this->createNewClientsFromFile();
                continue;
            case 4:
                $this->editClient();
                continue;
            case 5:
                $this->deleteClient();
                continue;
            case 6:
                exit();
            default :
                echo "\nInput incorrect. Please try again";
                continue;
            }
        } 
    }
    
    private function handleOptions()
    {   
        echo "\n";
        echo "\n Options available: ";
        echo "\n1. View existing clients ";
        echo "\n2. Add client ";
        echo "\n3. Add client from .csv file ";
        echo "\n4. Edit client information ";
        echo "\n5. Delete client ";
        echo "\n6. Exit application\n";
        
        return readline("Select option by typing option number: \n");
    }
    
    private function createNewClientFromTerminal()
    {
        if ($client = $this->createNewClient()) {
            $this->clients[] = $client;
            echo "\nClient created";
        } else {
            echo "\nFailed to create client";
        }
    }

    private function createNewClient()
    {
        $firstName = readline("Enter clients firstname: ");
        $lastName = readline("Enter clients lastname: ");
        $email = readline("Enter clients email: ");
        $phoneNumber1 = readline("Enter client first phone number: ");
        $phoneNumber2 = readline("Enter clients second phone number: ");
        $comment = readline("Add comment: ");
        
        $client = new Client(
            $firstName, 
            $lastName, 
            $email, 
            $phoneNumber1, 
            $phoneNumber2, 
            $comment
        );
        
        $errors = $client->errorsExist();
        
        if ($errors) {
            foreach ($errors as $error) {
                echo $error . "\n";
            }
         
            return false;
        }
        
        if ($this->findClientByEmail($client->getEmail())) {
            echo "\nEmail already exist";
            return false;
        }
        
        return $client;   
    }
    
    private function printAllClients()
    {
        if (empty($this->clients)) {
            echo "\nNo clients\n";
        }
        
        foreach ($this->clients as $client) {
            echo "\nFirstname:". $client->getFirstName() ;
            echo "\nLastname:".$client->getLastName();
            echo "\nEmail:". $client->getEmail();
            echo "\nPhone number one:" .$client->getPhoneNumber1();
            echo "\nPhone number two:" .$client->getPhoneNumber2();
            echo "\nComment:". $client->getComment();
            
            
            echo "\n";
            
        }
    }
    
    private function deleteClient()
    {
        $email = readline("Enter clients email: ");
        $key = $this->findClientByEmail($email);
        
        if (!is_null($key)) {
            unset($this->clients[$key]);
            echo "\nClient removed";
            
        } else {
            echo "\nEmail not found";
        }
    }
    
    private function editClient()
    {
        $email = readline("Enter clients email: ");
        $key = $this->findClientByEmail($email);
        
        if (!is_null($key) && $client = $this->createNewClient()) {
            $this->clients[$key] = $client;
            echo "\nClient updated";
        } else {
            echo "\nClient not found";
        }
    }
    
    private function createNewClientsFromFile()
    {
        $fileDestination = readline("Enter file path. (E.g.: c:\clients.csv): " );
        $clients = array_map('str_getcsv', file($fileDestination));
        
        foreach ($clients as $client) {
            $clientObject = new Client(
                    $client[0],
                    $client[1],
                    $client[2],
                    $client[3],
                    $client[4],
                    $client[5]
                );
            
            $errors = $clientObject->errorsExist();
        
            if ($errors) {
                foreach ($errors as $error) {
                    echo $error . "\n" ;
                }

                 continue;
            }

            if ($this->findClientByEmail($clientObject->getEmail())) {
                echo "\nEmail already exist:  " . $clientObject->getEmail();
                continue;
            }

            $this->clients[] = $clientObject;
            
            
        }
        echo "\nClients updated";
    }
    
    private function findClientByEmail($email)
    {  
        foreach ($this->clients as $key => $client) {
            if ($client->getEmail() == $email) {
                return $key;
            } 
        }
        
        return null;
        
    }
}
